package com.zak.cruise.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class User {
    @Id
    @GeneratedValue
    @Column
    private Integer id;
    @Column
    @NotNull
    private String username;
    @Column
    @NotNull
    private String surname;
    @Column
    @NotNull
    private String email;
    @Column
    @NotNull
    private String phoneNumber;
    @Column
    @NotNull
    private String country;
    @Column
    @NotNull
    private String city;
    @Column
    @NotNull
    private String address;
    @Column
    @NotNull
    private String zipCode;
    @Column
    @NotNull
    private String documentId;
//    @Column(name = "role_idrole")
//    @Value("1")
    @Column
    @NotNull
    private String password;
    @Column
    @NotNull
    private String login;
    //the role is first by default (1 - guest, 2 - moderator, 3 - admin)

    @Column
    private String photo;

    @Column
    private boolean active = true;

    public User() {
    }

    public User(Integer id, String username, String surname, String email, String phoneNumber, String country, String city, String address, String zipCode, String documentId, String password, String login) {
        this.id = id;
        this.username = username;
        this.surname = surname;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.city = city;
        this.address = address;
        this.zipCode = zipCode;
        this.documentId = documentId;
        this.password = password;
        this.login = login;
        this.active = true;
        this.photo = null;
    }

    public User(String username, String surname, String email, String phoneNumber, String country, String city, String address, String zipCode, String documentId, String password, String login) {
        this.username = username;
        this.surname = surname;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.city = city;
        this.address = address;
        this.zipCode = zipCode;
        this.documentId = documentId;
        this.password = password;
        this.login = login;
        this.active = true;
        this.photo = null;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return active;
    }
    @Transient
    public String getPhotosImagePath() {
        if (photo == null || id == null)
            return null;

        return "/user-photos/" + id + "/" + photo;
    }
}
