package com.zak.cruise.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Ship {
    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column
    private int uniqueId;

    @Column
    private String uniqueName;

    @Column
    private int capacity;

    @Column
    private int numberOfPorts;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Staff staffName;

    public Ship() {
    }

    public Ship(int uniqueId, String uniqueName, int capacity, int numberOfPorts, Staff staffName) {
        this.uniqueId = uniqueId;
        this.uniqueName = uniqueName;
        this.capacity = capacity;
        this.numberOfPorts = numberOfPorts;
        this.staffName = staffName;
    }
}
