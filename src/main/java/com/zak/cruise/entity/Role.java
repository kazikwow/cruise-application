package com.zak.cruise.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    @Value("1")
    private Long id;
    @Column
    private String role;

    public Role() {
    }

    public Role(String role) {
        this.role = role;
    }
}
