package com.zak.cruise.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Orders {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Cruise cruise;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Status status;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private User user;

    public Orders() {
    }

    public Orders(Cruise cruise, Status status, User user) {
        this.cruise = cruise;
        this.status = status;
        this.user = user;
    }

    public Orders(Long id, Cruise cruise, Status status, User user) {
        this.id = id;
        this.cruise = cruise;
        this.status = status;
        this.user = user;
    }

    public Long getId() {
        return id;
    }
}
