package com.zak.cruise.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Route {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String startPlace;

    @Column
    private String finishPlace;

    public Route() {
    }

    public Route(String startPlace, String finishPlace) {
        this.startPlace = startPlace;
        this.finishPlace = finishPlace;
    }
}
